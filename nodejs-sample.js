"use strict";

// Check Application Enviroment
var appEnv = process.env.NODE_ENV || 'production';
// => End

/*=============================================
=            Load Required Modules            =
=============================================*/
var express = require('express'),
    app = express(),
    cors = require('cors'),
    fs = require('fs'),
    session = require('express-session'),
    redisStore = require('connect-redis')(session),
    favicon = require('serve-favicon'),
    hbs = require("hbs"),
    chalk = require("chalk"),
    compression = require('compression'),
    methodOverride = require("method-override"),
    cookieParser = require("cookie-parser"),
    bodyParser = require("body-parser"),
    multer = require('multer'),
    chalk = require('chalk'),
    //NODEMO MODULES => Start
    configs = require("nodemo-configloader"),
    nodemoHash = require("nodemo-hash"),
    nodemoLogger = require("nodemo-logger"),
    nodemoUtils = require("nodemo-utils"),
    nodemoRedis = require("nodemo-redis"),
    nodemoMongodb = require("nodemo-mongodb"),
    nodemoRouter = require("nodemo-router"),   
    nodemoGoogleAuth = require("nodemo-googleauth");
    // => End
/*-----  End of Load Required Modules  ------*/

/*============================================
=            Set Custom Host/Port            =
============================================*/
if (process.env.HOST)
    configs.application.host = process.env.HOST;

if (process.env.PORT)
    configs.application.port = process.env.PORT;
// => END

/*============================================
=            Application Settings            =
============================================*/
app
    .disable("x-powered-by")
    .use(compression())
    .use(cors({
        origin: function(origin, callback) {
            var originIsWhitelisted = configs.site.urls.indexOf(origin) !== -1;
            if (origin)
            console.log(chalk.dim("cors: %s => %s"), originIsWhitelisted, origin);
            callback(null, originIsWhitelisted);
        }
    }))
    .use(express.static(configs.views.staticPath))
    .use(favicon(configs.views.favicon))
    .set("env", appEnv) //Added in 1.1.0
    .set("views", configs.paths.modules)
    .set("view engine", configs.views.engine)
    .set("view cache", appEnv === 'production') //Added in 1.1.0
    .set('trust proxy', false)  //changed in 1.1.0
    .engine("html", hbs.__express)
    .use(nodemoRedis())
    .use(session({
        store: new redisStore({
            host: configs.redis.host,
            port: configs.redis.port
        }),
        secret: configs.sessions.secret,
        key: configs.sessions.key,
        cookie: {
            httpOnly: true
        },
        resave: false,
        saveUninitialized: true
    }))
    .use(cookieParser())
    .use(methodOverride())
    .use(bodyParser.json())
    .use(bodyParser.urlencoded({
        extended: true
    }))
    .use(multer({
        dest: configs.uploads.path,
        onFileUploadStart: function(file, req, res) {
            console.log(chalk.dim('uploading ' + file.originalname + ' ...'));
        },
        onFileUploadData: function(file, data, req, res) {
            console.log(chalk.dim(data.length + ' of ' + file.originalname + ' uploaded.'));
        },
        onFileUploadComplete: function(file, req, res) {
            console.log(chalk.dim(file.originalname + ' uploaded to  ' + file.path));
        }
    }))
    .use(nodemoLogger)
    .use(nodemoUtils(app))
    .use(nodemoMongodb(appEnv))
    .use(nodemoGoogleAuth.googleAuth(app));
/*-----  End of Application Settings  ------*/


//ROUTING
nodemoRouter(app, appEnv);
// => END

//SET PROCESS UID
process.setuid(configs.application.user);
// => END

if (appEnv === "production") {
    module.exports = {
        app: app,
        env: appEnv,
        conf: configs.application
    }
} else {
    app.listen(configs.application.port, configs.application.host, function() {
        console.log(chalk.blue("%s is running on http://%s:%s [%s mode]"),
            configs.application.name,
            configs.application.host,
            configs.application.port,
            appEnv
        );
    });
}
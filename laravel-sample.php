<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Validator;
use Log;
use Response;
use App\Card;
use App\Transaction;
use App\Client;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cardlable' => 'required|min:3|max:255',
            'cardtoken' => 'required|unique:giftcards|max:255',
            'price' => 'required'
        ]);

        if ($validator->fails()) {
            return Response::json([
                "status" => "failed",
                "errors" => $validator->errors()->all()
            ], 200);
        }

        $card = new Card;
        $card->cardlable = $request->input('cardlable');
        $card->cardtoken = $request->input('cardtoken');
        $card->price     = $request->input('price');
        $card->save();
        if ($request->is('api/v1/*')) {
            return Response::json([
                "status" => "ok"
            ], 200);
        }
        return redirect('/panel?card=created');
    }

    public function getAll()
    {
        $cards = Card::all();

        return Response::json([
            "status" => "ok",
            "cards" => $cards
        ], 200);
    }

    public function listAll()
    {
        $cards = Card::where('expired', false)
            ->orderBy('cardlable')
            ->select('id', 'cardlable')
            ->get();
        
        return Response::json([
            "status" => "ok",
            "cards" => $cards
        ], 200);
    }

    public function buyOne(Request $request)
    {
        //validate transaction id
        $transId = $request->input('transid');
        $trans = Transaction::where('id', $transId)
            ->where('expired', 0)
            ->select('client_id', 'bank', 'created_at')
            ->take(1)
            ->get();

        if (!count($trans)) {
            return Response::json([
                "status" => "invalid_transaction"
            ], 200);
        }

        $client = Client::where('id', $trans[0]->client_id)
            ->select('email', 'tel')
            ->take(1)
            ->get();

        if (!count($client)) {
            return Response::json([
                "status" => "invalid_client"
            ], 200);
        }

        $card = Card::where('expired', 0)
            ->where('cardlable', $request->input('cardlable'))
            ->select('id', 'cardlable', 'cardtoken')
            ->orderBy('id')
            ->take(1)
            ->get();

        if (count($card)) {
            //update card
            $updCard = Card::find($card[0]->id);
            $updCard->trans_id = $transId;
            $updCard->expired = 1;
            $updCard->save();

            //update transaction
            $updTrans = Transaction::find($transId);
            $updTrans->expired = 1;
            $updTrans->save();
            
            return Response::json([
                "status" => "ok",
                "card" => $card[0]
            ], 200);
        } else {
            return Response::json([
                "status" => "card_not_exists"
            ], 200);
        }  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $card = Card::where('id', $id)
            ->take(1)
            ->get();

        if (count($card)) {
            Card::destroy($card[0]->id);
            if ($request->is('api/v1/*')) {
                return Response::json([
                    "status" => "ok",
                    "message" => "removed",
                ], 200);
            }
            return redirect('/panel?card=removed');
        } else {
            return Response::json([
                "status" => "card_not_exists"
            ], 200);
        }
    }
}
